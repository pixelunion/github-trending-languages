# Trending languages on Github

We'd like a little server-side service that lists the trending languages used in public Github repos.

Responses should return as JSON. For the sake of this excercise, pretend you're building an API that will be hooked up to some kind of single-page Javascript app, which will render this data in some kind of super impressive UI.


## Part 1

Fetch public Github repos, and return a JSON object that lists the most-used languages by those repos.

[API Reference](https://developer.github.com/v3/repos/#list-all-public-repositories). This response lists the 100 most recent repositories. We'll assume that's a wide enough sampling of the languages in use.

A `GET /api/trending` request should respond with a JSON object that looks something like:

```
{
	"languages": [
		{ "name": "Javascript" },
		{ "name": "C" },
		...
	]
}
```

## PART 2

Building on the first part, we should now be able to filter the results so that only repositories belonging to Github specific types of repos are used.

`GET /api/trending/?filter=user|organization|all`

```
{
	"matching_repos": 123,
	"languages": [
		{ "name": "Javascript" },
		{ "name": "C" },
		...
	]
}
```

## PART 3

Finally, we want to add some metadata about each language. In the JSON response, include a link to the language's Wikipedia article and a logo image.

```
{
	"matching_repos": 123,
	"languages": [
		{
			"name": "Javascript",
			"wikipedia": "https://en.wikipedia.org/wiki/JavaScript",
			"logo": "http://3.bp.blogspot.com/-PTty3CfTGnA/TpZOEjTQ_WI/AAAAAAAAAeo/KeKt_D5X2xo/s1600/js.jpg"
		},
		...
	]
}
```
